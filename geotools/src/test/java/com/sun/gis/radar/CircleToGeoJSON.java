package com.sun.gis.radar;

import org.geotools.data.*;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.*;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.locationtech.jts.geom.*;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;


import java.awt.geom.Point2D;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class CircleToGeoJSON {
    public static void main(String[] args) {
        try {
            // 南京的经纬度
            Coordinate centerCoord = new Coordinate(118.796877, 32.060255);
            double radius = 100000; // 100公里
            int numSectors = 36; // 总共36个扇形

            // 创建FeatureCollection
            SimpleFeatureType featureType = createFeatureType();
            DefaultFeatureCollection collection = new DefaultFeatureCollection("internal", featureType);
            populateFeatureCollection(collection, centerCoord, radius, numSectors);

            // 写入GeoJSON
            File file = new File("D:\\data\\radar\\nanjing_circle_sectors.geojson");
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                FeatureJSON fjson = new FeatureJSON();
                fjson.writeFeatureCollection(collection, writer);
            }
            System.out.println("GeoJSON file created: " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static SimpleFeatureType createFeatureType() {
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("Sector");
        builder.setCRS(DefaultGeographicCRS.WGS84); // 设置坐标参考系统
        builder.add("geometry", Polygon.class);
        builder.add("id", Integer.class);
        return builder.buildFeatureType();
    }

    private static void populateFeatureCollection(DefaultFeatureCollection collection, Coordinate center, double radius, int numSectors) {
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
        GeodeticCalculator calculator = new GeodeticCalculator();
        SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(createFeatureType());

        for (int i = 0; i < numSectors; i++) {
            double startAngle = i * (360.0 / numSectors);
            double endAngle = (i + 1) * (360.0 / numSectors);
            Polygon sector = createSector(geometryFactory, calculator, center, radius, startAngle, endAngle);
            if (sector != null && sector.isValid()) {
                featureBuilder.add(sector);
                featureBuilder.add(i);
                SimpleFeature feature = featureBuilder.buildFeature(String.valueOf(i));
                collection.add(feature);
            } else {
                System.err.println("Invalid geometry for sector " + i);
            }
        }
    }

    private static Polygon createSector(GeometryFactory geometryFactory, GeodeticCalculator calculator, Coordinate center, double radius, double startAngle, double endAngle) {
        CoordinateList coordinates = new CoordinateList();
        coordinates.add(center);
        calculator.setStartingGeographicPoint(center.x, center.y);

        int numPoints = 10; // 在圆弧上增加点以增强扇形的精确度
        for (int j = 0; j <= numPoints; j++) {
            double angle = startAngle + (j * (endAngle - startAngle) / numPoints);
            calculator.setDirection(angle, radius);
            Point2D dest = calculator.getDestinationGeographicPoint();
            coordinates.add(new Coordinate(dest.getX(), dest.getY()));
        }
        coordinates.add(center);
        coordinates.closeRing();
        return geometryFactory.createPolygon(coordinates.toCoordinateArray());
    }
}