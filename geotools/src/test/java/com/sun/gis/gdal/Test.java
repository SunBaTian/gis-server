package com.sun.gis.gdal;

import com.sun.gis.utils.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.gdal.gdal.gdal;
import org.gdal.ogr.DataSource;
import org.gdal.ogr.*;
import org.gdal.osr.SpatialReference;

/**
 * gdal测试类
 * @author ckk
 * @create 2022-08-06 16:07
 **/
@Slf4j
public class Test {

    public static void main(String[] args) {
        // 1.dxf 转 geojson
        boolean b = dxf2GeoJson("D:\\data\\gdal\\", "12.dxf", "D:\\data\\gdal\\");
        System.out.println("bbbbbb==="+b);
    }

    /**
     * dxf转geojson（矢量拆分）
     * @param inputPath
     * @param cadFileName
     * @param outputPath
     * @return
     */
    public static boolean dxf2GeoJson(String inputPath, String cadFileName, String outputPath){
        try {
            ogr.RegisterAll();
            // gdal.SetConfigOption 选项配置参见：https://trac.osgeo.org/gdal/wiki/ConfigOptions
            // 为了支持中文路径，请添加下面这句代码
            gdal.SetConfigOption("GDAL_FILENAME_IS_UTF8", "YES");
            // 为了使属性表字段支持中文，请添加下面这句
            gdal.SetConfigOption("SHAPE_ENCODING", "");
            gdal.SetConfigOption("DXF_ENCODING", "ASCII");
            /**
             * 判断编码dxf文件编码类型：
             * 在cad另存为dxf时，由于不同版本问题导致编码不同。
             * 已知：dxf >=2007 版本编码为 UTF-8，其他低版本编码为 GB2312
             * 若为 UTF-8 需要设置：gdal.SetConfigOption("DXF_ENCODING", "ASCII");
             */
//            String charset = EncodingDetector.getCharset(new File(inputPath + cadFileName));
//            if(null != charset && charset.equals("UTF-8")){
//                //设置DXF缺省编码
//                gdal.SetConfigOption("DXF_ENCODING", "ASCII");
//            }

//        String inputPath = "F:\\ckk\\mapbox研究\\java集成gdal\\xjlmk.dxf";
            DataSource ds = ogr.Open(inputPath + cadFileName, 0);
            if (ds == null) {
                log.info("打开dxf文件失败！filePath="+inputPath + cadFileName);
                return false;
            }
            log.info("打开dxf文件成功！");

            // 拆分矢量文件为多个单要素矢量文件,注意拆分后的fid需要重置，
            String fieldName = null;
//        String saveFolderPath = "F:\\ckk\\mapbox研究\\java集成gdal\\xjlmk-1628561264202\\";// 生成的矢量文件保存目录
            String saveFolderPath = outputPath;// 生成的矢量文件保存目录

            boolean dirExists = FileUtil.judgeDirExists(saveFolderPath, true);
            if(!dirExists){
                log.error("dxf2GeoJson，文件目录创建失败！saveFolderPath ="+saveFolderPath);
                return false;
            }

            Driver dv = ogr.GetDriverByName("GeoJSON");
            if (dv == null) {
                log.error("打开GeoJSON驱动失败！");
                return false;
            }
            log.info("打开GeoJSON驱动成功！");

            Layer layer = ds.GetLayer(0);
            SpatialReference spatial = layer.GetSpatialRef();
            int geomType = layer.GetGeomType();
            FeatureDefn layerDefn = layer.GetLayerDefn();
            int fieldCount = layerDefn.GetFieldCount();
            Feature feature = layer.GetNextFeature();
            int count = 1;
            while(feature != null){
//                Feature featureCopy = feature.Clone();
                String outName = "", outLayerName = "";
                if(fieldName ==  null){
                    Long fid = feature.GetFID();
//                    int fid = feature.GetFID();
                    outName = String.valueOf(fid);
                    outLayerName = String.valueOf(fid);
                }else{
                    outLayerName = feature.GetFieldAsString(fieldName);
                    outName = outLayerName;
                }
                if(outName == null){
                    continue;
                }
                String outFileName = outName + ".geojson";
                int dir = count % 100;// 需要分页储存，因为geojson文件过多，严重影响写入和读取效率。
                String subDir = saveFolderPath + dir + "/";
                if(!FileUtil.judgeDirExists(subDir, true)){
                    log.error("dxf2GeoJson，文件子目录创建失败！subDir ="+subDir);
                    return false;
                }
                String outFilePath = subDir + outFileName;
                DataSource outData = dv.CreateDataSource(outFilePath);
                Layer outLayer = outData.CreateLayer(outLayerName, spatial, geomType);
                for(int i=0; i<fieldCount; i++){
                    FieldDefn fieldDefn = layerDefn.GetFieldDefn(i);
                    outLayer.CreateField(fieldDefn);
                }
//                outLayer = outData.GetLayer();
                feature.SetFID(0l);
                outLayer.CreateFeature(feature);
                feature = layer.GetNextFeature();
                // 此处不执行delete()的话，会导致json文件生成缺少结尾
                if (outLayer != null) {
                    outLayer.delete();
                }
                if (outData != null) {
                    outData.delete();
                }

                count++;
            }
            // 可选
//            gdal.GDALDestroyDriverManager();// 此处会导致 java bug
            System.out.println("dxf2GeoJson完毕！！");
            return true;
        }catch (Exception e){
            e.printStackTrace();
            log.error("异常：方法dxf2GeoJson！");
            return false;
        }finally {
//            if (ds != null) { // 此处会导致 java bug
//                ds.delete();
//            }
//            if (outLayer != null) {
//                outLayer.delete();
//            }
//            if (outData != null) {
//                outData.delete();
//            }
        }
    }

}