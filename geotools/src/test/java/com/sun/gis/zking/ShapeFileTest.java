package com.sun.gis.zking;

import com.sun.gis.tools.shapefile.ShapefileProcessor;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.feature.DefaultFeatureCollection;
import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.File;
import java.nio.charset.StandardCharsets;

public class ShapeFileTest {


    public static void main(String[] args) {



        // 输入文件
        String inputShapeFile = "C:\\Users\\Administrator.WIN-3IRDTT6AC3U\\Desktop\\公司项目\\紫金农险\\紫金数据\\5个旗县的林班成果数据\\呼伦贝尔南木林业局\\out\\23146215070223000002_utf8_4326.shp";
        // 输出文件路径
        String outputShapeFile = "C:\\Users\\Administrator.WIN-3IRDTT6AC3U\\Desktop\\公司项目\\紫金农险\\紫金数据\\5个旗县的林班成果数据\\呼伦贝尔南木林业局\\handle\\23146215070223000002.shp";


        try {
            // 读取输入的 Shapefile
            File file = new File(inputShapeFile);
            FileDataStore store = FileDataStoreFinder.getDataStore(file);
            ((ShapefileDataStore) store).setCharset(StandardCharsets.UTF_8); // 设置编码为 UTF-8
            SimpleFeatureSource featureSource = store.getFeatureSource();

            // 获取 Shapefile 的所有特征
            SimpleFeatureCollection collection = featureSource.getFeatures();

            // 创建新的特征集合用于存储修改后的特征
            DefaultFeatureCollection newCollection = new DefaultFeatureCollection();
            SimpleFeatureType sft = featureSource.getSchema();

            // 遍历所有特征
            try (SimpleFeatureIterator features = collection.features()) {
                while (features.hasNext()) {
                    SimpleFeature feature = features.next();

                    // 假设 stitchFeature 是自定义方法，用于处理特征
                    Polygon stitchedPolygon = ShapefileProcessor.stitchFeature(feature);

                    // 创建新的特征并添加到新的集合中
//                    SimpleFeature newFeature = ShapefileProcessor.createNewSimpleFeature(stitchedPolygon, sft);
                    SimpleFeature newFeature = ShapefileProcessor.createNewSimpleFeature(stitchedPolygon, feature, sft);
                    newCollection.add(newFeature);
                }
            }

            // 保存修改后的特征到新的 Shapefile
            ShapefileProcessor.saveAsShapefile(newCollection, outputShapeFile);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
