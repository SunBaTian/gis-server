package com.sun.gis.geojson;

import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.locationtech.jts.geom.Geometry;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import java.io.StringReader;

public class AreaTest {

    public static void main(String[] args) {
        area();
    }

    public static void area() {
        // GeoJSON字符串
        String geoJsonString = "{\n" +
                "        \"coordinates\": [\n" +
                "          [\n" +
                "            [\n" +
                "              117.65541075627914,\n" +
                "              35.80737832400399\n" +
                "            ],\n" +
                "            [\n" +
                "              117.65541075627914,\n" +
                "              35.793173440330705\n" +
                "            ],\n" +
                "            [\n" +
                "              117.67284491734586,\n" +
                "              35.793173440330705\n" +
                "            ],\n" +
                "            [\n" +
                "              117.67284491734586,\n" +
                "              35.80737832400399\n" +
                "            ],\n" +
                "            [\n" +
                "              117.65541075627914,\n" +
                "              35.80737832400399\n" +
                "            ]\n" +
                "          ]\n" +
                "        ],\n" +
                "        \"type\": \"Polygon\"\n" +
                "      }";

        // 从GeoJSON字符串读取Feature对象
        Geometry geometry = null;
        try {
            GeometryJSON gjson = new GeometryJSON();
            geometry = gjson.read(new StringReader(geoJsonString));
            // 定义源坐标参考系统 (WGS84, EPSG:4326)
            CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:4326");

            // 定义目标坐标参考系统 (China 2000, EPSG:4528)
            CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:4528");

            // 转换几何对象到目标投影
            MathTransform transform = CRS.findMathTransform(sourceCRS, targetCRS, true);
            Geometry transformedGeometry = JTS.transform(geometry, transform);
            // 计算转换后几何对象的面积
            double area = transformedGeometry.getArea(); // 默认单位为平方米
            System.out.println("Area in square meters: " + area);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
