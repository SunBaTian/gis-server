package com.sun.gis.database;

import com.sun.gis.entity.City;

import com.sun.gis.mapper.CityMapper;
import com.sun.gis.service.ICityService;
import com.sun.gis.tools.photo.TestScreenshot1;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.styling.*;
import org.geotools.styling.Stroke;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.io.WKTReader;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.awt.*;
import java.io.IOException;
import java.util.List;

/**
 * @author sunbt
 * @date 2024/1/3 23:41
 */
@SpringBootTest
public class TestPhoto {

    @Resource
    ICityService iCityService;

    @Resource
    CityMapper cityMapper;

    @Test
    public void test() {
        List<String> wktStrList = cityMapper.getWktStr();
        System.out.println(wktStrList.size());
        // 创建一个GeometryFactory来生成Geometry对象
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();

// 创建特征类型构建器
        SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
        typeBuilder.setName("MultiPolygonFeature");
        typeBuilder.setCRS(DefaultGeographicCRS.WGS84); // 设置坐标参考系统
        typeBuilder.add("geometry", MultiPolygon.class); // 添加Geometry字段

// 构建特征类型
        SimpleFeatureType featureType = typeBuilder.buildFeatureType();

// 创建特征集合
        DefaultFeatureCollection featureCollection = new DefaultFeatureCollection("internal", featureType);

// WKT读取器
        WKTReader reader = new WKTReader(geometryFactory);

        for (String wkt : wktStrList) {
            try {
                Geometry geometry = reader.read(wkt);
                SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(featureType);
                featureBuilder.add(geometry);
                SimpleFeature feature = featureBuilder.buildFeature(null);
                featureCollection.add(feature);
            } catch (Exception e) {
                System.out.println("Error reading WKT: " + e.getMessage());
            }
        }

        // 创建样式
        Style style = SLD.createSimpleStyle(featureType);
        Style customStyle = TestScreenshot1.createCustomStyle();
        // 创建Layer
        Layer layer = new FeatureLayer(featureCollection, customStyle);

        String outputPath="E:\\code\\gitee\\gis-server\\data\\output\\photo\\map_view2.png";
        try {
            TestScreenshot1.getFourScreenshots(layer,outputPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
