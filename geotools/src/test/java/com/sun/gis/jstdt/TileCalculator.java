package com.sun.gis.jstdt;

public class TileCalculator {

    public static void main(String[] args) {
        testJs();
//        double[] fullExtent = {-180, -90, 180, 90};
//        double width4490 = fullExtent[2] - fullExtent[0];  // 经度差
//        double height4490 = fullExtent[3] - fullExtent[1]; // 纬度差
//
//        int tileWidth = 256;  // 瓦片宽度，通常为256像素
//        int tileHeight = 256; // 瓦片高度，通常为256像素
//
//        double[] resolutions4490 = new double[18]; // 保存从层级1到17的分辨率
//        int maxZoom = 17; // 最大层级
//
//        // 计算每个层级的分辨率
//        for (int z = 1; z <= maxZoom; z++) {
//            resolutions4490[z] = width4490 / (tileWidth * Math.pow(2, z));
//        }
//
//        // 计算每个层级的行列号范围
//        for (int z = 1; z <= maxZoom; z++) {
//            int numCols = (int) Math.ceil(width4490 / (resolutions4490[z] * tileWidth));
//            int numRows = (int) Math.ceil(height4490 / (resolutions4490[z] * tileHeight));
//            System.out.println("Zoom level " + z + ":");
//            System.out.println("  Columns range: 0 to " + (numCols - 1));
//            System.out.println("  Rows range: 0 to " + (numRows - 1));
//        }
    }


    public static void test() {
        double[] fullExtent = {-180, -90, 180, 90};
        double width4490 = fullExtent[2] - fullExtent[0];  // 经度差
        double height4490 = fullExtent[3] - fullExtent[1]; // 纬度差

        int tileWidth = 256;
        int tileHeight = 256;

        double[] resolutions4490 = new double[18];
        int maxZoom = 17;

        for (int z = 0; z <= maxZoom; z++) {
            if (z == 0) {
                resolutions4490[z] = width4490 / tileWidth;  // 默认层级0分辨率
            } else {
                resolutions4490[z] = width4490 / (tileWidth * Math.pow(2, z));
            }

            int numCols = (int) Math.ceil(width4490 / (resolutions4490[z] * tileWidth));
            int numRows = (int) Math.ceil(height4490 / (resolutions4490[z] * tileHeight));

            System.out.println("Zoom level " + z + ":");
            System.out.println("  Columns range: 0 to " + (numCols - 1));
            System.out.println("  Rows range: 0 to " + (numRows - 1));
        }
    }

    public static void testJs(){
        // 定义新的地图范围
        double[] customExtent = {
                116.10358013377254,  // 最小经度
                30.710719079012677, // 最小纬度
                122.09030402444137, // 最大经度
                35.21265930204362   // 最大纬度
        };

        double width4490 = customExtent[2] - customExtent[0];  // 经度差
        double height4490 = customExtent[3] - customExtent[1]; // 纬度差

        int tileWidth = 256;  // 瓦片宽度，通常为256像素
        int tileHeight = 256; // 瓦片高度，通常为256像素

        double[] resolutions4490 = new double[18]; // 保存从层级0到17的分辨率
        int maxZoom = 17; // 最大层级

        // 计算每个层级的分辨率
        for (int z = 0; z <= maxZoom; z++) {
            resolutions4490[z] = width4490 / (tileWidth * Math.pow(2, z));
        }

        // 计算每个层级的行列号范围
        for (int z = 0; z <= maxZoom; z++) {
            int numCols = (int) Math.ceil(width4490 / (resolutions4490[z] * tileWidth));
            int numRows = (int) Math.ceil(height4490 / (resolutions4490[z] * tileHeight));
            System.out.println("Zoom level " + z + ":");
            System.out.println("  Columns range: 0 to " + (numCols - 1));
            System.out.println("  Rows range: 0 to " + (numRows - 1));
        }
    }

}




