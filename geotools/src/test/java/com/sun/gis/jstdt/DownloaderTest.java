package com.sun.gis.jstdt;

import com.sun.gis.tools.jstdt.TileDownloader;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
// 多线程下载，代码逻辑有问题，多线程消耗在了下载任务上，太多的打开和关闭下载流，方法会内存溢出，阻塞CPU
@SpringBootTest
public class DownloaderTest {

    @Resource
    ExecutorService executorService;

    @Test
    public void test() {

        String baseUrl = "https://jiangsu.tianditu.gov.cn/historyraster/rest/services/History/yxdt_js_1966_2k/MapServer/tile/";
        String outputDir = "F:\\data\\jiangsu\\1966";

        File directory = new File(outputDir);
        if (!directory.exists()) {
            directory.mkdirs(); // 创建目录如果它不存在
        }

        double[] fullExtent = {-180, -90, 180, 90};
        double[] realExtent = {
                116.10358013377254,  // 最小经度
                30.710719079012677, // 最小纬度
                122.09030402444137, // 最大经度
                35.21265930204362   // 最大纬度
        };
        double width4490 = fullExtent[2] - fullExtent[0];  // 经度差
        double height4490 = fullExtent[3] - fullExtent[1]; // 纬度差

        int tileWidth = 256;  // 瓦片宽度，通常为256像素
        int tileHeight = 256; // 瓦片高度，通常为256像素

        double[] resolutions4490 = new double[19]; // 保存从层级1到19的分辨率
        int maxZoom = 18; // 最大层级

        // 计算每个层级的分辨率
        for (int z = 1; z <= maxZoom; z++) {
            resolutions4490[z] = width4490 / (tileWidth * Math.pow(2, z));
        }
        for (int z = 7; z <= maxZoom; z++) {
            int numCols = (int) Math.ceil(width4490 / (resolutions4490[z] * tileWidth));
            int numRows = (int) Math.ceil(height4490 / (resolutions4490[z] * tileHeight));
            for (int x = 0; x < numCols; x++) {
                for (int y = 0; y < numRows; y++) {
                    double tileMinX = -180 + x * resolutions4490[z] * tileWidth;
                    double tileMaxX = tileMinX + resolutions4490[z] * tileWidth;
                    double tileMaxY = 90 - y * resolutions4490[z] * tileHeight;
                    double tileMinY = tileMaxY - resolutions4490[z] * tileHeight;
                    if (tileMinX < realExtent[2] && tileMaxX > realExtent[0] &&
                            tileMinY < realExtent[3] && tileMaxY > realExtent[1]) {
                        final String outputFilePath = outputDir + File.separator + "tile_" + z + "_" + y + "_" + x + ".png";
                        File tileFile = new File(outputFilePath);
                        if (tileFile.exists()) {
                            System.out.println("Tile already exists and will be skipped: " + outputFilePath);
                        } else {
                            final String tileUrl = baseUrl + z + "/" + y + "/" + x;
                            int finalZ = z;
                            int finalY = y;
                            int finalX = x;
                            executorService.execute(() -> {
                                try {
                                    System.out.println("Downloading with thread: " + Thread.currentThread().getName());
                                    TileDownloader.downloadTile(tileUrl, outputFilePath);
                                    System.out.println("Successfully downloaded: " + outputFilePath);
                                } catch (Exception e) {
                                    System.err.println("Failed to download tile " + finalZ + "/" + finalY + "/" + finalX + ": " + e.getMessage());
                                }
                            });
                        }
                    }
                }
            }
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
