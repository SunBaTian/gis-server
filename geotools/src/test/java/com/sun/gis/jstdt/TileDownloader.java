package com.sun.gis.jstdt;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import java.io.File;


public class TileDownloader {



// downloadTile 方法保持不变


    public static void main(String[] args) {
        String baseUrl = "https://jiangsu.tianditu.gov.cn/mapjs2/rest/services/MapJS/js_yxdt_latest/MapServer/tile/";
        String outputDir = "F:\\data\\jiangsu\\2023";
        File directory = new File(outputDir);
        if (!directory.exists()) {
            directory.mkdirs(); // 创建目录如果它不存在
        }

        double[] fullExtent = {-180, -90, 180, 90};
        double width4490 = fullExtent[2] - fullExtent[0];  // 经度差
        double height4490 = fullExtent[3] - fullExtent[1]; // 纬度差

        int tileWidth = 256;  // 瓦片宽度，通常为256像素
        int tileHeight = 256; // 瓦片高度，通常为256像素

        double[] resolutions4490 = new double[20]; // 保存从层级1到17的分辨率
        int maxZoom = 19; // 最大层级

        // 计算每个层级的分辨率
        for (int z = 1; z <= maxZoom; z++) {
            resolutions4490[z] = width4490 / (tileWidth * Math.pow(2, z));
        }

        for (int z = 8; z <= maxZoom; z++) {
            int numCols = (int) Math.ceil(width4490 / (resolutions4490[z] * tileWidth));
            int numRows = (int) Math.ceil(height4490 / (resolutions4490[z] * tileHeight));
            for (int x = 0; x < numCols; x++) {
                for (int y = 0; y < numRows; y++) {
                    final String tileUrl = baseUrl + z + "/" + y + "/" + x;
                    final String outputFilePath = outputDir + File.separator + "tile_" + z + "_" + y + "_" + x + ".png";
                    try {
                        downloadTile(tileUrl, outputFilePath);
                        System.out.println("Successfully downloaded: " + outputFilePath);
                    } catch (Exception e) {
                        System.err.println("Failed to download tile " + z + "/" + y + "/" + x + ": " + e.getMessage());
                    }

                }
            }
            System.out.println("Scheduled downloads for zoom level " + z);
        }


    }

    private static void downloadTile(String urlString, String outputFile) throws Exception {
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(15000); // 15 seconds
        connection.setReadTimeout(15000); // 15 seconds

        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            try (InputStream inputStream = connection.getInputStream();
                 FileOutputStream outputStream = new FileOutputStream(outputFile)) {
                byte[] buffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
            }
        } else {
            throw new Exception("Server returned non-OK status: " + connection.getResponseCode());
        }
        connection.disconnect();
    }
}

