package com.sun.gis.mapper;

import com.sun.gis.entity.City;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sung
 * @since 2024-01-03
 */
public interface CityMapper extends BaseMapper<City> {

    /**
     * 获取wkt
     *
     * @return list
     */
    List<String> getWktStr();

}
