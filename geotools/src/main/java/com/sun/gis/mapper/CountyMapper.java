package com.sun.gis.mapper;

import com.sun.gis.entity.County;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sung
 * @since 2024-01-03
 */
public interface CountyMapper extends BaseMapper<County> {

}
