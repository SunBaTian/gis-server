package com.sun.gis.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sung
 * @since 2024-01-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class County implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String geom;

    private Long objectid;

    private String name;

    private String pac;


}
