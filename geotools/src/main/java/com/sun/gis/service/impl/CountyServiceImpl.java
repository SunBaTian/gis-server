package com.sun.gis.service.impl;

import com.sun.gis.entity.County;
import com.sun.gis.mapper.CountyMapper;
import com.sun.gis.service.ICountyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sung
 * @since 2024-01-03
 */
@Service
public class CountyServiceImpl extends ServiceImpl<CountyMapper, County> implements ICountyService {

}
