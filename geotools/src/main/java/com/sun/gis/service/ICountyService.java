package com.sun.gis.service;

import com.sun.gis.entity.County;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sung
 * @since 2024-01-03
 */
public interface ICountyService extends IService<County> {

}
