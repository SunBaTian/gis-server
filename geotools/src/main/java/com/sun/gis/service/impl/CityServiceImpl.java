package com.sun.gis.service.impl;

import com.sun.gis.entity.City;
import com.sun.gis.mapper.CityMapper;
import com.sun.gis.service.ICityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sung
 * @since 2024-01-03
 */
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City> implements ICityService {

}
