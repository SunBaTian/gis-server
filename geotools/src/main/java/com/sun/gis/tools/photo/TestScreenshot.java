package com.sun.gis.tools.photo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;

import org.locationtech.jts.geom.*;
import org.opengis.feature.simple.SimpleFeature;

/**
 * 截图测试（未完成）
 *
 * @author sunbt
 * @date 2024/1/3 15:56
 */
public class TestScreenshot {

    public static void main(String[] args) {
        try {
            // 读取Shapefile
            File file = new File("E:\\data\\GIS\\division\\4326\\huaian.shp");
            FileDataStore store = FileDataStoreFinder.getDataStore(file);
            SimpleFeatureCollection features = store.getFeatureSource().getFeatures();

            // 计算最小外接矩形
            Envelope envelope = features.getBounds();

            // 创建包含所有图斑的最小外接矩形
            GeometryFactory geometryFactory = new GeometryFactory();
            Polygon rectangle = geometryFactory.createPolygon(new Coordinate[]{
                    new Coordinate(envelope.getMinX(), envelope.getMinY()),
                    new Coordinate(envelope.getMinX(), envelope.getMaxY()),
                    new Coordinate(envelope.getMaxX(), envelope.getMaxY()),
                    new Coordinate(envelope.getMaxX(), envelope.getMinY()),
                    new Coordinate(envelope.getMinX(), envelope.getMinY())
            });

            // 创建图像
            BufferedImage image = new BufferedImage(800, 600, BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics = image.createGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, image.getWidth(), image.getHeight());

            // 绘制图斑和最小外接矩形
            SimpleFeatureIterator iterator = features.features();
            while (iterator.hasNext()) {
                SimpleFeature feature = iterator.next();
                MultiPolygon multiPolygon = (MultiPolygon) feature.getDefaultGeometry();
                drawGeometry(graphics, multiPolygon, Color.RED); // 绘制图斑
            }
            drawGeometry(graphics, rectangle, Color.RED); // 绘制最小外接矩形

            // 保存图像到文件
            File output = new File("E:\\code\\gitee\\gis-server\\data\\output\\photo\\output.png");
            javax.imageio.ImageIO.write(image, "png", output);

            // 释放资源
            iterator.close();
            graphics.dispose();
            store.dispose();

            System.out.println("Image generated successfully!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 绘制Geometry
    private static void drawGeometry(Graphics2D graphics, Geometry geometry, Color color) {
        graphics.setColor(color);
        for (int i = 0; i < geometry.getNumGeometries(); i++) {
            Geometry subGeometry = geometry.getGeometryN(i);
            if (subGeometry instanceof Polygon) {
                Polygon polygon = (Polygon) subGeometry;
                Coordinate[] coordinates = polygon.getExteriorRing().getCoordinates();
                int[] xPoints = new int[coordinates.length];
                int[] yPoints = new int[coordinates.length];
                for (int j = 0; j < coordinates.length; j++) {
                    xPoints[j] = (int) coordinates[j].x;
                    yPoints[j] = (int) coordinates[j].y;
                }
                graphics.drawPolygon(xPoints, yPoints, coordinates.length);
            }
        }
    }

}
