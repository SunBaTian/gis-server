package com.sun.gis.tools.wmts;

/**
 * @author sunbt
 * @date 2023/9/12 23:39
 */
public class WmtsTileInfo {
    public int x;
    public int y;
    public int zoom;
    public String layer;
    public String style;
    public String tileMatrixSet;

    public WmtsTileInfo(int x, int y, int zoom, String layer, String style, String tileMatrixSet) {
        this.x = x;
        this.y = y;
        this.zoom = zoom;
        this.layer = layer;
        this.style = style;
        this.tileMatrixSet = tileMatrixSet;
    }

}
