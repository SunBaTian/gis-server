package com.sun.gis.tools.dataFormat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * EsriJson和GeoJson数据格式互转工具
 */
public class EsriJsonGeoJsonTransformTools {


    /**
     * esriJson转GeoJson
     *
     * @param esriJson esriJson
     * @return GeoJson
     */
    public static JSONObject esriJsonToGeoJson(JSONObject esriJson) {
        JSONObject geoJson = new JSONObject();
        geoJson.put("type", "FeatureCollection");
        JSONArray features = new JSONArray();

        JSONArray esriFeatures = esriJson.getJSONArray("features");
        for (int i = 0; i < esriFeatures.size(); i++) {
            JSONObject esriFeature = esriFeatures.getJSONObject(i);
            JSONObject geoFeature = new JSONObject();
            geoFeature.put("type", "Feature");
            geoFeature.put("geometry", esriFeature.getJSONObject("geometry"));
            geoFeature.put("properties", esriFeature.getJSONObject("attributes"));
            features.add(geoFeature);
        }
        geoJson.put("features", features);

        return geoJson;
    }

    /**
     * geoJson转EsriJson
     *
     * @param geoJson geoJson
     * @return EsriJson
     */
    public static JSONObject geoJsonToEsriJson(JSONObject geoJson) {
        JSONObject esriJson = new JSONObject();
        JSONArray esriFeatures = new JSONArray();

        JSONArray geoFeatures = geoJson.getJSONArray("features");
        for (int i = 0; i < geoFeatures.size(); i++) {
            JSONObject geoFeature = geoFeatures.getJSONObject(i);
            JSONObject esriFeature = new JSONObject();
            esriFeature.put("geometry", geoFeature.getJSONObject("geometry"));
            esriFeature.put("attributes", geoFeature.getJSONObject("properties"));
            esriFeatures.add(esriFeature);
        }
        esriJson.put("features", esriFeatures);

        return esriJson;
    }

    public static void main(String[] args) {
        // EsriJSON 示例数据
        String esriJsonStr = "{"
                + "\"features\": ["
                + "  {"
                + "    \"geometry\": {\"x\": 125.6, \"y\": 10.1},"
                + "    \"attributes\": {\"name\": \"点A\"}"
                + "  },"
                + "  {"
                + "    \"geometry\": {\"paths\": [[[100.0, 0.0], [101.0, 1.0]]]},"
                + "    \"attributes\": {\"name\": \"线B\"}"
                + "  }"
                + "]"
                + "}";

        JSONObject esriJsonObj = JSON.parseObject(esriJsonStr);

        // EsriJSON 转 GeoJSON
        JSONObject geoJsonObj = EsriJsonGeoJsonTransformTools.esriJsonToGeoJson(esriJsonObj);
        System.out.println("EsriJSON to GeoJSON: ");
        System.out.println(geoJsonObj.toJSONString());

        // GeoJSON 示例数据
        String geoJsonStr = "{"
                + "\"type\": \"FeatureCollection\","
                + "\"features\": ["
                + "  {"
                + "    \"type\": \"Feature\","
                + "    \"geometry\": {\"type\": \"Point\", \"coordinates\": [125.6, 10.1]},"
                + "    \"properties\": {\"name\": \"点A\"}"
                + "  },"
                + "  {"
                + "    \"type\": \"Feature\","
                + "    \"geometry\": {\"type\": \"LineString\", \"coordinates\": [[100.0, 0.0], [101.0, 1.0]]},"
                + "    \"properties\": {\"name\": \"线B\"}"
                + "  }"
                + "]"
                + "}";

        JSONObject geoJsonObjInput = JSON.parseObject(geoJsonStr);

        // GeoJSON 转 EsriJSON
        JSONObject esriJsonObjOutput = EsriJsonGeoJsonTransformTools.geoJsonToEsriJson(geoJsonObjInput);
        System.out.println("GeoJSON to EsriJSON: ");
        System.out.println(esriJsonObjOutput.toJSONString());
    }

}
