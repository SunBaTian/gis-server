package com.sun.gis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@EnableAsync
//@Configuration
public class ThreadPoolConfig {
//    @Bean
//    public ExecutorService executorService() {
//        // 创建一个固定大小的线程池
//        return Executors.newFixedThreadPool(4); // 可以根据需求调整线程池的大小
//    }
}
