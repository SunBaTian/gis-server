package com.sun.gis.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sung
 * @since 2024-01-03
 */
@RestController
@RequestMapping("/county")
public class CountyController {

}
